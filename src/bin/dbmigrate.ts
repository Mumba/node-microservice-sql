#!/usr/bin/env node
// Database migrations
// @copyright 2017 Mumba Pty Ltd. All rights reserved.
// @license   Apache-2.0

const knex = require('knex');
const chalk = require('chalk');
const commandLineArgs = require('command-line-args');
import * as url from 'url';
import {GenericSecret} from 'mumba-vault';
import {Config, FileLoader} from 'mumba-config';
import {VaultLoader} from 'mumba-config-vault';
const Retry = require('retry-if').default;

const args: any[] = commandLineArgs([
	{
		name: 'config',
		type: String
	},
	{
		name: 'vault',
		type: String
	},
	{
		name: 'vault-token',
		type: String
	}
]);

function spread(f: Function) {
	return (array: any[]) => f(...array);
}

function getConfig(options: any = {}): Promise<any> {
	const config = new Config();
	const loaders: any[] = [];

	if (options.config) {
		config.registerLoader(new FileLoader());

		loaders.push({
			name: 'file',
			filePath: options.config
		});
	}

	if (options['vault'] && options['vault-token']) {
		const uri = url.parse(options['vault']);

		config.registerLoader(new VaultLoader(new GenericSecret({
			host: uri.protocol + '//' + uri.hostname,
			port: +uri.port || 8200
		})));

		loaders.push({
			name: 'vault',
			token: options['vault-token'],
			path: uri.pathname
		});
	}

	return config.load(loaders)
		.then(() => config);
}

getConfig(args)
	.then((config: Config) => {
		const options = {
			client: 'mysql2',
			connection: {
				host: config.get('db:host'),
				database: config.get('db:database'),
				user: config.get('db:username'),
				password: config.get('db:password')
			},
			migrations: {
				directory: './dbmigrations',
				tableName: config.get('db:knex')
			},
			seeds: {
				directory: './dbseeds'
			}
		};

		const retry = new Retry({
			growthRate: 1000,
			maxRetry: 10
		});

		retry.if((err: Error) => {
			console.error('Retrying knex:', err);

			return true;
		});

		return retry.try(() => {
			return knex(options).migrate.latest();
		})
			.exec();

	})
	.then(spread((batchNo: any, log: any) => {
		if (log.length === 0) {
			console.error(chalk.cyan('Already up to date'));
		}
		console.error(chalk.green('Batch ' + batchNo + ' run: ' + log.length + ' migrations \n' + chalk.cyan(log.join('\n'))));
		process.exit(0);
	}))
	.catch((err: Error) => {
		console.error(chalk.red(err instanceof Error ? err.message : err));
		process.exit(1);
	});

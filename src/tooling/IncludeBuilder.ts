/**
 * @copyright 2017 Mumba Pty Ltd. All rights reserved.
 * @license   Apache-2.0
 */

import {ConditionBuilder} from './ConditionBuilder';

/**
 * Condition builder to handle specifics for an association.
 *
 * @constructor
 */
export class IncludeBuilder extends ConditionBuilder {
	private _model: any = null;
	private _as: any = null;
	private _all: any = null;
	private _nested: any = null;
	private _required: boolean = null;
	private _through: ConditionBuilder = null;

	/**
	 * Build the condition.
	 *
	 * @return {Object}
	 */
	public build() {
		const build = super.build.apply(this);

		if (this._model) {
			build.model = this._model;
		}

		if (this._as) {
			build.as = this._as;
		}

		if (this._all) {
			build.all = true;
		}

		if (this._nested) {
			build.nested = true;
		}

		if (this._required) {
			build.required = true;
		}

		if (this._through) {
			build.through = this._through.build();
		}

		return build;
	}

	/**
	 * Set the model for the association.
	 *
	 * @param {Object} value - A Sequelize model.
	 * @returns {IncludeBuilder} this
	 */
	public model(value: any) {
		this._model = value;

		return this;
	}

	/**
	 * Set the `as` model alias for the association.
	 *
	 * @param {String} value - The as alias.
	 * @returns {IncludeBuilder} this
	 */
	public as(value: string) {
		this._as = value;

		return this;
	}

	/**
	 * Set the `all` flag.
	 *
	 * @returns {IncludeBuilder} this
	 */
	public all() {
		this._all = true;

		return this;
	}

	/**
	 * Set the `nested` flag.
	 *
	 * @returns {IncludeBuilder} this
	 */
	public nested() {
		this._nested = true;

		return this;
	}

	/**
	 * Set the `required` flag.
	 *
	 * @returns {IncludeBuilder} this
	 */
	public required() {
		this._required = true;

		return this;
	}

	/**
	 * Set the `through` conditions.
	 *
	 * @returns {IncludeBuilder} this
	 */
	public through(through: ConditionBuilder) {
		this._through = through;

		return this;
	}
}

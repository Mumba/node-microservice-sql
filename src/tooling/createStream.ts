/**
 * @copyright Mumba Pty Ltd 2015. All rights reserved.
 * @license   Apache-2.0
 */

const Readable = require('stream').Readable;

/**
 * Create a stream from a Sequelize select query.
 *
 * @param {Function} query          - A function that returns a promise and takes a Sequelize query options as an argument.
 * @param {object}   options        - The options to inject into the query function.
 * @param {number}   options.limit  - The limit for the number of rows to return.
 * @param {number}   options.offset - The row offset to start at.
 * @returns {stream.Readable}
 * @throws if options.limit or options.offset is not supplied.
 */
export function createStream(query: (options: any) => Promise<any>, options: any) {
	const stream = new Readable({ objectMode: true });

	if (options.limit === void 0) {
		throw new Error('createSequelizeStream: <options.limit> required.');
	}

	if (options.offset === void 0) {
		throw new Error('createSequelizeStream: <options.offset> required.');
	}

	stream._read = function () {
		// tslint:disable-next-line
		this.pause();

		query(options)
			.then((rows) => {
				if (rows.length === 0) {
					// tslint:disable-next-line
					this.push(null);
				}
				else {
					rows.forEach((row: any) => {
						// tslint:disable-next-line
						this.push(row);
					});
					options.offset += options.limit;
				}

				// tslint:disable-next-line
				this.resume();
			})
			.catch((err: Error) => {
				options.offset += options.limit;
				// tslint:disable-next-line
				this.resume();
				// tslint:disable-next-line
				this.emit('error', err);
			});
	};

	return stream;
}

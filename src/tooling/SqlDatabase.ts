/**
 * @copyright 2017 Mumba Pty Ltd. All rights reserved.
 * @license   Apache-2.0
 */

const Sequelize = require('sequelize');

/**
 * Provides overarching support for a SQL database.
 */
export class SqlDatabase {
	private db: any;

	/**
	 * @param {object} options
	 * @param {string} options.database - Default: memory.
	 * @param {string} options.username
	 * @param {string} options.password
	 * @param {string} [options.host]
	 * @see {@link http://sequelize.readthedocs.org/en/latest/api/sequelize/#class-sequelize} for more options
	 */
	constructor(options: any = {}) {
		this.db = new Sequelize(options.database || 'memory', options.username, options.password, options);
	}

	/**
	 * Add a model to the database.
	 *
	 * @param {Function} factory - A factory function to create a model.
	 * @returns {any}
	 */
	public addModel(factory: Function) {
		return factory(this.db);
	}

	/**
	 * Add a dictionary of models to the database.
	 *
	 * @param {Function} factory - A factory function to create a model.
	 * @returns {void}
	 */
	public addModels(dict: any) {
		Object.keys(dict).forEach((key: string) => {
			this.addModel(dict[key]);
		});
	}

	/**
	 * Get the dictionary of all the models in the database.
	 *
	 * @returns {any}
	 */
	public getModels(): any {
		return this.db.models;
	}

	/**
	 * Get a single model defined in the database.
	 *
	 * @param {String} name
	 * @returns {any}
	 */
	public getModel(name: string): any {
		return this.db.models[name];
	}

	/**
	 * Syncronise
	 *
	 * USE WITH CAUTION (NOT A GOOD IDEA IN PRODUCTION).
	 *
	 * @param {Object}  [options]
	 * @param {boolean} [options.force] - Force changes.
	 * @returns {any|Promise<Model<TInstance, TAttributes>>|Promise<*>}
	 */
	public sync(options?: any) {
		return this.db.sync(options);
	}

	/**
	 * Initialise models.
	 */
	public initialise() {
		const models = this.getModels();

		Object.keys(models).forEach((key) => {
			const model = models[key];

			if (model.associate) {
				model.associate(models);
			}
		});
	}

	/**
	 * Perform a raw query.
	 *
	 * @returns {any|Promise<*>}
	 */
	public query(sql: string | { query: string, values: any[] }, options?: any) {
		return this.db.query(sql, options);
	}

	public transaction(callback: Function) {
		return this.db.transaction(callback);
	}
}

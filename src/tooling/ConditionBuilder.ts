/**
 * @copyright 2017 Mumba Pty Ltd. All rights reserved.
 * @license   Apache-2.0
 */

import * as sequelize from 'sequelize';

const Op = sequelize.Op;

const DEFAULT_LIMIT = 10;

function notEmpty(value: any) {
	return value !== void 0 && value !== null;
}

/**
 * Sequelize condition builder.
 *
 * @constructor
 */
export class ConditionBuilder {
	private _path: any = null;
	private _where = new Map<any, any>();
	private _attributes: any = null;
	private _limit: any = null;
	private _offset: number;
	private _includes: any[] = [];
	private _order: any[] = [];
	private _paranoid: boolean;
	private options: any = {};

	/**
	 * Build the condition.
	 *
	 * @returns {Object}
	 */
	public build(): any {
		const build: any = {};

		if (this._where.size) {
			build.where = {};
			Array.from(this._where)
				.forEach(([key, value]) => build.where[key] = value);
		}

		if (this._attributes !== null) {
			build.attributes = this._attributes;
		}

		if (this._includes.length > 0) {
			build.include = [];

			this._includes.forEach(function (include) {
				build.include.push(include.build());
			});
		}

		if (this._order.length > 0) {
			build.order = this._order;
		}

		if (notEmpty(this._limit)) {
			build.limit = this._limit;
		}

		if (notEmpty(this._offset)) {
			build.offset = this._offset;
		}

		if (notEmpty(this._paranoid)) {
			build.paranoid = this._paranoid;
		}

		Object.keys(this.options)
			.forEach(key => build[key] = this.options[key]);

		return build;
	}

	public set(name: string, value: any) {
		this.options[name] = value;

		return this;
	}

	/**
	 * Include an association condition.
	 *
	 * @param {IncludeBuilder} value
	 * @returns {ConditionBuilder}
	 */
	public include(value: ConditionBuilder) {
		if (!(value instanceof ConditionBuilder)) {
			throw new Error('ConditionBuilder.include: <value> not an instance of `ConditionBuilder`.');
		}

		this._includes.push(value);

		return this;
	}

	/**
	 * Set the limit for the condition.
	 *
	 * @param {Number} value
	 * @returns {ConditionBuilder} this
	 */
	public limit(value: number = DEFAULT_LIMIT) {
		this._limit = +value;

		return this;
	}

	/**
	 * Set the offset for the condition.
	 *
	 * @param {Number} value
	 * @returns {ConditionBuilder} this
	 */
	public offset(value: number) {
		this._offset = +value || 0;

		return this;
	}

	/**
	 * Set the page for the condition.
	 *
	 * @param {Number} value
	 * @returns {ConditionBuilder} this
	 */
	public page(value: number) {
		if (value && this._limit > 0) {
			this.offset(this._limit * Math.max(value - 1, 0));
		}

		return this;
	}

	/**
	 * Adds a where condition.
	 *
	 * @param {String} key
	 * @param {Object} [value]
	 * @returns {ConditionBuilder} this
	 */
	public where(key: string, value?: any) {
		if (value === undefined) {
			this._path = key;
		}
		else {
			this._where.set(key, value);
		}

		return this;
	}

	/**
	 * Specifies the complementary comparison value for paths specified with `where()`.
	 *
	 * @param {Object} value
	 * @returns {ConditionBuilder} this
	 */
	public equals(value: any) {
		this._assertPath('equals');
		this._where.set(this._path, value);

		return this;
	}

	/**
	 * Specifies the attributes (columns) to be returned.
	 *
	 * @param {Array} value
	 * @returns {ConditionBuilder} this
	 */
	public attributes(value: any[]) {
		return this.set('attributes', value);
	}

	/**
	 * Specifies the ordering clause(s).
	 *
	 * @param {String} column
	 * @param {String} direction
	 * @returns {ConditionBuilder} this
	 */
	public order(column: string, direction: string) {
		const args = Array.prototype.slice.call(arguments);

		this._order.push(args);

		return this;
	}

	/**
	 * Merges a conditional expression.
	 *
	 * @param {String} operator
	 * @param {Object} value
	 * @returns {ConditionBuilder} this
	 * @private
	 */
	public conditional(operator: string, value: any) {
		this._assertPath(operator);

		const conditions = this._where.has(this._path)
			? this._where.get(this._path)
			: this._where.set(this._path, {}).get(this._path);

		conditions[(<any>Op)[operator]] = value;

		return this;
	}

	/**
	 * Specifies a greater-than query condition.
	 *
	 * @param {number|string|Date} value
	 * @returns {ConditionBuilder} this
	 */
	public gt(value: number | string | Date) {
		return this.conditional('gt', value);
	}

	/**
	 * Specifies a greater-than-or-equal query condition.
	 *
	 * @param {number|string|Date} value
	 * @returns {ConditionBuilder} this
	 */
	public gte(value: number | string | Date) {
		return this.conditional('gte', value);
	}

	/**
	 * Specifies a less-than query condition.
	 *
	 * @param {number|string|Date} value
	 * @returns {ConditionBuilder} this
	 */
	public lt(value: number | string | Date) {
		return this.conditional('lt', value);
	}

	/**
	 * Specifies a less-than-or-equal query condition.
	 *
	 * @param {number|string|Date} value
	 * @returns {ConditionBuilder} this
	 */
	public lte(value: number | string | Date) {
		return this.conditional('lte', value);
	}

	/**
	 * Specifies a not-equal query condition.
	 *
	 * @param {string|number|Date} value
	 * @returns {ConditionBuilder} this
	 */
	public ne(value: string | number | Date) {
		return this.conditional('ne', value);
	}

	/**
	 * Specifies an in query condition.
	 *
	 * @param {Array} value
	 * @returns {ConditionBuilder} this
	 */
	public in(value: any[]) {
		return this.conditional('in', value);
	}

	/**
	 * Specifies a not-in query condition.
	 *
	 * @param {Array} value
	 * @returns {ConditionBuilder} this
	 */
	public notIn(value: any[]) {
		return this.conditional('notIn', value);
	}

	public paranoid(value: boolean) {
		return this.set('paranoid', !!value);
	}

	/**
	 * Assert the `_path` property is set.
	 *
	 * @param {String} method
	 * @private
	 */
	protected _assertPath(method: string) {
		if (!this._path) {
			throw new Error(method + '() must be used after where()');
		}
	}
}

/**
 * @copyright 2017 Mumba Pty Ltd. All rights reserved.
 * @license   Apache-2.0
 */

import {ValidationErrors} from 'mumba-errors';
import * as sequelize from 'sequelize';
import {CreateOptions, UpdateOptions} from 'sequelize';
import Instance = sequelize.Instance;
import Model = sequelize.Model;
import {ConditionBuilder} from './ConditionBuilder';

export interface FindIdsFilters {
	ids?: any[];
}

export interface FindPageFilters {
	offset?: number;
	limit?: number;
}

/**
 * Add conditions for querying on the primary key.
 */
function addIdsConditions(conds: ConditionBuilder, options: FindIdsFilters, key: string = 'id') {
	if (Array.isArray(options.ids) && options.ids.length > 0) {
		conds.where(key, options.ids);
	}
}

/**
 * Add conditions for pagination.
 */
function addPagingConditions(conds: ConditionBuilder, options: FindPageFilters) {
	if (+options.limit) {
		conds.limit(+options.limit);
	}

	if (+options.offset) {
		conds.offset(+options.offset);
	}
}

export type TModel<A> = Model<Instance<A>, A>;

export class SqlDataService<TAttributes, TInstance> {
	private pk = 'id';

	constructor(private dataModel: TModel<TAttributes>) {
		if (!dataModel) {
			throw new Error('SqlDataService: <dataModel> required.');
		}
	}

	/**
	 * Get the model factory.
	 */
	public getModel(): TModel<TAttributes> {
		return this.dataModel;
	}

	/**
	 * Add data.
	 */
	public add(values: TAttributes, options?: CreateOptions): Promise<TInstance | ValidationErrors> {
		return new Promise((resolve) => {
			const errors = this.validateAdd(values);

			resolve(errors.any() ? errors: this.getModel().create(values, options));
		}) as any;
	}

	public count(options: any = {}): Promise<number> {
		return new Promise((resolve) => {
			resolve(this.getModel().count(this.getFindAllConditions(options).build()));
		});
	}

	/**
	 * Get a list of data matching search criteria.
	 */
	public get(options: FindIdsFilters | FindPageFilters): Promise<Instance<TAttributes>[]> {
		return this.getModel()
			.findAll(this.getFindAllConditions(options).build()) as any;
	}

	/**
	 * Set properties on existing data.
	 */
	public set(values: any, options?: UpdateOptions): Promise<Instance<TAttributes> | ValidationErrors> {
		return this.getModel()
			.findById(values[this.pk])
			.then((instance) => {
				if (!instance) {
					return <any>(new ValidationErrors()).notFound(this.pk);
				}

				const errors = this.validateSet(values, instance.get());

				return errors.any() ? errors : instance.update(values, options);
			}) as any;
	}

	/**
	 * Delete data by id. Checks that the instance first exists.
	 */
	public delete(id: number): Promise<TAttributes> {
		return this.getModel()
			.findById(id)
			.then((instance) => {
				if (!instance) {
					return <any>(new ValidationErrors()).notFound(this.pk);
				}

				return instance.destroy()
					.then(() => instance.get());
			}) as any;
	}

	protected primaryKey(name: string) {
		this.pk = name;
	}

	/**
	 * Validate inputs for add.
	 */
	protected validateAdd(values: TAttributes): ValidationErrors {
		return new ValidationErrors();
	}

	/**
	 * Validate inputs for add.
	 */
	protected validateSet(newValues: TAttributes, oldValues: TAttributes): ValidationErrors {
		return new ValidationErrors();
	}

	/**
	 * Get the query options for finding a list of data.
	 */
	protected getFindAllConditions(options: FindIdsFilters | FindPageFilters): ConditionBuilder {
		const conds = new ConditionBuilder();

		addPagingConditions(conds, options as FindPageFilters);
		addIdsConditions(conds, options as FindIdsFilters, this.pk);

		return conds;
	}
}

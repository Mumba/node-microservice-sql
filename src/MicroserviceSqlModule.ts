/**
 * @copyright 2017 Mumba Pty Ltd. All rights reserved.
 * @license   Apache-2.0
 */

import {Config} from 'mumba-config';
import {DiContainer} from 'dicontainer';
import {SqlDatabase} from './tooling/SqlDatabase';

export class MicroserviceSqlModule {
	public static register(container: DiContainer) {
		container.factory('db', (config: Config, done: Function) => {
			const options = config.get('db');
			const db = new SqlDatabase(options);

			if (!container.has('dbModels')) {
				return done(null, db);
			}

			container.resolve((err: Error, dbModels: any) => {
				db.addModels(dbModels);
				db.initialise();

				// Can only sync if he have models.
				if (options.sync && options.sync.force) {
					return db.sync(options.sync)
						.then(() => done(null, db))
						.catch(done);
				}

				done(null, db);
			});
		});
	}
}


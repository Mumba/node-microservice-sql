[![build status](https://gitlab.com/Mumba/node-microservice-sql/badges/master/build.svg)](https://gitlab.com/Mumba/node-microservice-sql/commits/master)
# Mumba Microservice SQL

Provide SQL support (via Sequelize) for a Microservice.

## Configuration

This module requires that a `Config` object has been registered as `config`.

Property | Type | Description
--- | :---: | ---
`db` | `object` | A dictionary of options required for [Sequelize](http://sequelize.readthedocs.io/en/latest/api/sequelize/#class-sequelize).
`db:sync:force` | `boolean` | Set to true if you want the database to perform a force sync (normally only used for testing). Default: `false`. 

## Models

Models need to be added to the `dbModels` group in the container. For example:

```
container.object('fooModel', fooModelFunction, ['dbModels']);
```

## DI Container

Name | Type | Description
--- | :---: | ---
`db` | `SqlDatabase` | A class that wraps the Sequelize database object.
`dbModels` | `object` | A dictionary of the database models.

## Installation

```sh
$ npm install --save mumba-microservice-sql
$ typings install --save dicontainer=npm:mumba-typedef-dicontainer
```

## Examples

TODO

## Database migrations

### Requirements

Database migrations for MySQL are provided via the [Knex](http://knexjs.org) package. In order to use this, you will need to install []MySQL](https://www.npmjs.com/package/mysql) into your Microservice. It's also a good idea to install Knex globally so you can use some of its command-line tools.
 
```sh
$ npm install --save mysql
$ npm install -g knex 
```

In addition, your Microservice configuration must specify the name of the Knex changelog table under the `db:knex` path. For example:
 
```typescript
let config: any = {
	db: {
		// ... other Sequelize database configuration.
		knex: '__knex'
	}
};

export default config;
```

> NOTE: If you are using a shared database, the changelog table needs to be unique for each Microservice otherwise Knex will get confused about what migration has or hasn't run yet. For example, if you have an _Articles_ Microservice, an appropriate name of the table would be `__knex_articles`.

### Creating migrations

If you've installed Knex globally, you can create a new migration script by running:

```sh
$ knex migrate:make migration_name
```

where `migration_name` is a textual label for the migration.

### Running migrations

To run the migrations during Microservice initialisation, include the following line in the `entrypoint.sh` file:

```sh
>&2 echo "Running database migrations"
/src/node_modules/.bin/dbmigrate --config=/src/dist/etc/config.js --vault-token=${VAULT_TOKEN} --vault=http://vault:8200/v1/secret/${CLIENT_DOMAIN}/common
```

Note that the expected arguments are:

* **Required** Path to the JavaScript configuration file.
* An optional Vault access token.
* An optional Vault secret path (required is the Vault token is supplied).

## Tests

To run the test suite, first install the dependencies, then run `npm test`:

```sh
$ npm run docker:up
$ npm install
$ npm test
$ npm docker:down
```

## People

The original author of _Mumba Microservice Vault_ is [Andrew Eddie](https://gitlab.com/u/aeddie.mumba).

[List of all contributors](https://gitlab.com/Mumba/node-microservice-vault/graphs/master)

## License

[Apache 2.0](LICENSE.txt)

* * *

&copy; 2017 [Mumba Pty Ltd](http://www.mumba.cloud). All rights reserved.


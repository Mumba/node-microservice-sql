const config: any = {
	db: {
		logging: false,
		dialect: 'mysql',
		username: 'root',
		password: 'root',
		database: 'dbmigrations',
		define: {
			freezeTableName: true,
			underscored: false
		},
		operatorsAliases: false,
		sync: {
			force: false
		},
		knex: '__knex'
	}
};

export default config;

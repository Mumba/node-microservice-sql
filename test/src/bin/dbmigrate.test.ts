/**
 * dbmigrate tests.
 *
 * Remember to observe the 4 A's of testing and try to limit tests to just four calls:
 * - Arrange    - set up the system state
 * - Act        - do the thing we are testing
 * - Assert     - inspect the resulting state
 * - Annihilate - tear down
 *
 * @copyright 2017 Mumba Pty Ltd. All rights reserved.
 * @license   Apache-2.0
 */

import * as path from 'path';
import {execFile} from 'child_process';

describe('dbmigrate', () => {
	it('should work', (done) => {
		execFile('node', [
			path.join(__dirname, '/../../../src/bin/dbmigrate.js'),
			'--config=' + path.join(__dirname, '/../../config.js')
		], (error: any, stdout: any, stderr: any) => {
			if (error) {
				console.error('STDERR:', stderr);
				console.error('STDOUT:', stdout);

				return done(error);
			}

			console.error('STDOUT:', stdout);
			done();
		})
	});
});

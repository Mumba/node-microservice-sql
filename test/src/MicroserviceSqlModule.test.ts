/**
 * MicroserviceVaultModule tests
 *
 * Remember to observe the 4 A's of testing and try to limit tests to just four calls:
 * - Arrange    - set up the system state
 * - Act        - do the thing we are testing
 * - Assert     - inspect the resulting state
 * - Annihilate - tear down
 *
 * @copyright 2017 Mumba Pty Ltd. All rights reserved.
 * @license   Apache-2.0
 */

const Sandal = require('sandal');
import * as assert from 'assert';
import {Config} from 'mumba-config';
import {DiContainer} from 'mumba-typedef-dicontainer';
import {MicroserviceSqlModule} from '../../src/index';
import {SqlDatabase} from '../../src/tooling/SqlDatabase';

const fooModel = (sequelize: any) => {
	return sequelize.define('foo', {});
};

describe('MicroserviceSqlModule integration tests', () => {
	let config: Config;
	let container: DiContainer;

	beforeEach(() => {
		config = new Config({
			db: {
				logging: false,
				dialect: 'sqlite',
				define: {
					timestamps: false,
					freezeTableName: true,
					underscored: false
				},
				operatorsAliases: false
			}
		});
		container = new Sandal();
		container.object('config', config);

		MicroserviceSqlModule.register(container);
	});

	it('should register artifacts in the container', (done) => {

		assert.strictEqual(container.has('db'), true, 'should have db');

		// TODO Add a TypeDef for a logger.
		container.resolve((err: Error, db: SqlDatabase) => {
			if (err) {
				return done(err);
			}

			assert(db instanceof SqlDatabase, 'should be a SqlDatabase object');
			done();
		});
	});

	it('should register models in the SqlDatabase if defined', (done) => {
		container.object('foo', fooModel, ['dbModels']);

		assert.strictEqual(container.has('dbModels'), true, 'should have the model dictionary');

		container.resolve((err: Error, db: SqlDatabase) => {
			if (err) {
				return done(err);
			}

			assert(db.getModel('foo'), 'should have the model');
			done();
		});
	});

	it('should associate models', (done) => {
		let associated = false;
		const assocModel = (sequelize: any) => {
			const model = sequelize.define('assoc', {});

			model.associate = (models: any) => {
				assert(models.assoc, 'should be the models');
				associated = true;
			};

			return model;
		};

		container.object('assoc', assocModel, ['dbModels']);
		container.resolve((err: Error, db: SqlDatabase) => {
			if (err) {
				return done(err);
			}

			assert(associated === true, 'should have the associated the model');
			done();
		});
	});

	it('should initialise the database if force sync is set', (done) => {
		container.object('foo', fooModel, ['dbModels']);
		config.set('db:sync:force', true);

		container.resolve((err: Error, db: SqlDatabase) => {
			if (err) {
				return done(err);
			}

			db.getModel('foo').findAll()
				.then((results: any[]) => {
					assert.strictEqual(Array.isArray(results), true, 'should be an array of results');
					done();
				})
				.catch(done);
		});
	});
});

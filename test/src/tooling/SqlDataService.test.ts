/**
 * Remember to observe the 4 A's of testing and try to limit tests to just four calls:
 * - Arrange    - set up the system state
 * - Act        - do the thing we are testing
 * - Assert     - inspect the resulting state
 * - Annihilate - tear down
 *
 * @copyright 2017 Mumba Pty Ltd. All rights reserved.
 * @license   Apache-2.0
 */

import * as assert from 'assert';
import {ValidationErrors} from 'mumba-errors';
import {SqlDataService} from '../../../src/tooling/SqlDataService';
import * as sequelize from 'sequelize';
import Model = sequelize.Model;
import Instance = sequelize.Instance;

interface TAttributes {
	id?: number;
	foo?: string;
}

interface SimpleInstance extends Instance<TAttributes>, TAttributes {
}

type SimpleModel = Model<SimpleInstance, TAttributes>;

class ValidatingService extends SqlDataService<TAttributes, SimpleInstance> {
	protected validateAdd(values: any) {
		const errors = super.validateAdd(values);

		return errors.required('add');
	}

	protected validateSet(newValues: any, oldValues: any) {
		const errors = super.validateSet(newValues, oldValues);

		if (newValues.foo !== oldValues.foo) {
			errors.invalid('foo');
		}

		return errors;
	}
}

class KeyedService extends SqlDataService<TAttributes, SimpleInstance> {
	constructor(model: SimpleModel) {
		super(model);
		this.primaryKey('keyId');
	}
}

describe('SqlDataService unit tests', function () {
	let instance: SqlDataService<TAttributes, SimpleInstance>;
	let model: SimpleModel;

	beforeEach(() => {
		model = <SimpleModel>{};
		instance = new SqlDataService(model);
	});

	it('should throw if `DataModel` missing in constructor', () => {
		assert.throws(() => {
			instance = new SqlDataService(void 0);
		}, /<dataModel> required/);
	});

	it('should get the model', () => {
		assert.strictEqual(instance.getModel(), model, 'should be the model factory');
	});

	it('should add valid data', () => {
		const data = { foo: 'bar' };
		const opts = { raw: false };

		model.create = (values: any, options: any): any => new Promise((resolve) => {
			assert.deepEqual(options, opts, 'should be the options');
			resolve(values);
		});

		return instance.add(data, opts)
			.then((result) => {
				assert.deepEqual(result, data, 'should pass the data through create');
			});
	});

	it('should return an error adding invalid data', () => {
		const data = { foo: 'bar' };
		const validatingInstance = new ValidatingService(model);

		return validatingInstance.add(data)
			.then((result) => {
				assert.equal(result.toJSON().$errors[0].property, 'add', 'should be an error for the calling method');
			});
	});

	it('should count the data', () => {
		const filters = {
			ids: [1, 2]
		};
		const expected = 5;

		model.count = (options: any): any => new Promise((resolve) => {
			assert.deepEqual(options.where.id, filters.ids, 'should be the filters');
			resolve(expected);
		});

		return instance.count(filters)
			.then((result) => {
				assert.equal(result, expected, 'should be the count');
			});
	});

	it('should get data, filtering by ids', () => {
		const filters = {
			ids: [1, 2]
		};

		model.findAll = (options: any) => <any>new Promise((resolve) => {
			assert.deepEqual(options.where.id, filters.ids, 'should be the ids filter');

			resolve(options.where.id.map((id: any) => {
				return { id };
			}));
		});

		return instance.get(filters)
			.then((results: any[]) => {
				assert.equal(results[0].id, 1, 'should be the result');
			});
	});

	it('should get data, filtering by custom id', () => {
		const keyedInstance = new KeyedService(model);
		const filters = {
			ids: [1, 2]
		};

		model.findAll = (options: any) => <any>new Promise((resolve) => {
			assert.deepEqual(options.where.keyId, filters.ids, 'should be the ids filter');

			resolve([]);
		});

		return keyedInstance.get(filters);
	});

	it('should get data with pagination', () => {
		const filters = {
			limit: 9,
			offset: 8
		};

		model.findAll = (options: any) => <any>new Promise((resolve) => {
			assert.equal(options.limit, filters.limit, 'should be the limit');
			assert.equal(options.offset, filters.offset, 'should be the offset');

			resolve([]);
		});

		return instance.get(filters);
	});

	it('should set valid data', () => {
		const data = { id: 1 };
		const opts = { where: <any>{} };

		model.findById = (id: any) => <any>Promise.resolve({
			get: (): any => Object.assign(data, {
				id,
				foo: 'bar'
			}),
			update: (values: any, options: any): any => new Promise((resolve) => {
				assert.deepEqual(options, opts, 'should be the update options');
				resolve(values);
			})
		});

		return instance.set(data, opts)
			.then((result: any) => {
				assert.equal(result.id, data.id, 'should pass the data through create');
				assert.equal(result.foo, 'bar', 'should merge properties from the data source');
			});
	});

	it('should set valid data with custom key', () => {
		const keyedInstance = new KeyedService(model);
		const data = { keyId: 1 };

		model.findById = (keyId: any) => <any>Promise.resolve({
			get: (): any => Object.assign(data, {
				keyId
			}),
			update: (values: any): any => Promise.resolve(values)
		});

		return keyedInstance.set(data)
			.then((result: any) => {
				assert.equal(result.keyId, data.keyId, 'should pass the data through create');
			});
	});

	it('should return a validation error if set cannot find the existing data', () => {
		const data = { id: 1 };

		model.findById = (id: any) => <any>Promise.resolve(null);

		return instance.set(data)
			.then((result: ValidationErrors) => {
				assert.equal(result.toJSON().$errors[0].type, 'not_found', 'should be a not found error');
			});
	});

	it('should return a validation error if setting invalid data', () => {
		const data = {
			id: 1,
			foo: 'gar'
		};
		const validatingInstance = new ValidatingService(model);

		model.findById = (id: any) => <any>Promise.resolve({
			get: () => new Object({
				id,
				foo: 'bar'
			})
		});

		return validatingInstance.set(data)
			.then((result) => {
				assert.equal(result.toJSON().$errors[0].property, 'foo', 'should be an error for the calling method');
			});
	});

	it('should delete records by id', () => {
		const values = { id: 1 };

		model.findById = (id: any) => <any>Promise.resolve({
			get: (): any => Object.assign({}, values),
			destroy: (): any => Promise.resolve()
		});

		return instance.delete(values.id)
			.then((result: any) => {
				assert.equal(result.id, values.id, 'should return the original data');
			});
	});

	it('delete should return a validation error if existing data is missing', () => {
		const values = { id: 1 };

		model.findById = (id: any) => <any>Promise.resolve(null);

		return instance.delete(values.id)
			.then((result) => {
				assert.equal((result as ValidationErrors).toJSON().$errors[0].type, 'not_found', 'should be a not found error');
			});
	});
});

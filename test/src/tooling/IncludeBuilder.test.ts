/**
 * Remember to observe the 4 A's of testing and try to limit tests to just four calls:
 * - Arrange    - set up the system state
 * - Act        - do the thing we are testing
 * - Assert     - inspect the resulting state
 * - Annihilate - tear down
 *
 * @copyright 2017 Mumba Pty Ltd. All rights reserved.
 * @license   Apache-2.0
 */

import * as assert from 'assert';
import {ConditionBuilder, IncludeBuilder} from '../../../src/index';

describe('IncludeBuilder', function () {
	let instance: IncludeBuilder;

	beforeEach(function () {
		instance = new IncludeBuilder();
	});

	it('should be an instance of `ConditionBuilder`', function () {
		assert(instance instanceof ConditionBuilder);
	});

	it('should set the `model`', function () {
		assert.deepEqual(instance.model({}).build().model, {});
	});

	it('should set the `as`', function () {
		assert.deepEqual(instance.as('foo').build().as, 'foo');
	});

	it('should set the `all` flag', function () {
		assert.deepEqual(instance.all().build(), { all: true });
	});

	it('should set the `nested` flag', function () {
		assert.deepEqual(instance.nested().build(), { nested: true });
	});

	it('should set the `required` flag', function () {
		assert.deepEqual(instance.required().build(), { required: true });
	});

	it('should set the `through` condition', function () {
		const through = new ConditionBuilder();

		through.where('foo', 'bar');

		assert.deepEqual(instance.through(through).build(), {
			through: {
				where: {
					foo: 'bar'
				}
			}
		});
	});
});

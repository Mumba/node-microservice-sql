/**
 * Remember to observe the 4 A's of testing and try to limit tests to just four calls:
 * - Arrange    - set up the system state
 * - Act        - do the thing we are testing
 * - Assert     - inspect the resulting state
 * - Annihilate - tear down
 *
 * @copyright Mumba Pty Ltd 2017. All rights reserved.
 * @license   Apache-2.0
 */

const through = require('through');

import * as assert from 'assert';
import {createStream} from '../../../src/index';

describe('createStream unit tests', () => {
	const source: any[] = [
		{ id: 1 },
		{ id: 2 },
		{ id: 3 },
		{ id: 4 }
	];
	let options: any;

	beforeEach(() => {
		options = {
			limit: 1,
			offset: 0
		};
	});

	it('should throw if options.limit missing', () => {
		delete options.limit;

		assert.throws(() => {
			createStream(({}) => Promise.resolve(void 0), options);
		}, /<options.limit> required/);
	});

	it('should throw if options.offset missing', () => {
		delete options.offset;

		assert.throws(() => {
			createStream(({}) => Promise.resolve(void 0), options);
		}, /<options.offset> required/);
	});

	it('should work', function (done) {
		// Simulate a findAll.
		const query = function (options: any) {
			return Promise.resolve(source.slice(options.offset, options.offset + options.limit));
		};

		const stream = createStream(query, { offset: 0, limit: 1 });
		let count = 0;

		stream
			.on('error', done)
			.pipe(through(function (data: any) {
				count++;
				assert.equal(data.id, count);
			}))
			.on('error', done)
			.pipe(through(null, () => {
				assert.equal(count, 4);
				done();
			}));
	});

	it('should handle an error', (done) => {
		const query = (options: any): Promise<any> => {
			if (options.offset === 1) {
				return Promise.reject(new Error('an-error'));
			}

			return Promise.resolve(source.slice(options.offset, options.offset + options.limit));
		};

		const stream = createStream(query, {
			offset: 0,
			limit: 1
		});
		let count = 0;

		stream
			.on('error', (err: Error) => {
				assert.equal(err.message, 'an-error');
				done();
			});

		stream.pipe(through((data: any) => {
			count++;
		}))
			.on('error', done);
	});
});

/**
 * Remember to observe the 4 A's of testing and try to limit tests to just four calls:
 * - Arrange    - set up the system state
 * - Act        - do the thing we are testing
 * - Assert     - inspect the resulting state
 * - Annihilate - tear down
 *
 * @copyright 2017 Mumba Pty Ltd. All rights reserved.
 * @license   Apache-2.0
 */

const assert = require('assert');
const Sequelize = require('sequelize');

import {SqlDatabase} from '../../../src/index';

const simpleModel = (sequelize: any) => {
	return sequelize.define('foo', {});
};

const simpleModel2 = (sequelize: any) => {
	return sequelize.define('bar', {});
};

describe('SqlDatabase unit tests', function () {
	let options: any;
	let instance: any;

	beforeEach(function () {
		options = {
			logging: false,
			dialect: 'sqlite',
			define: {
				timestamps: false,
				freezeTableName: true,
				underscored: false
			}
		};
		instance = new SqlDatabase(options);
	});

	it('should return model class when adding a model', () => {
		const Model = instance.addModel(simpleModel);

		assert.equal(Model.name, 'foo');
	});

	it('should be able to get a single model', () => {
		instance.addModel(simpleModel);

		assert.equal(instance.getModel('foo').name, 'foo');
	});

	it('should be able to add a dictionary of models', () => {
		instance.addModels({
			1: simpleModel,
			2: simpleModel2
		});

		assert.equal(instance.getModel('foo').name, 'foo');
		assert.equal(instance.getModel('bar').name, 'bar');
	});

	it('should be able to get a dictionary of all models', () => {
		instance.addModel(simpleModel);

		const models = instance.getModels();

		assert(models.foo, 'test model should exist');
	});

	it('should be able to sync and query the database', () => {
		instance.addModel(simpleModel);

		return instance.sync({ force: true })
			.then(() => {
				return instance.query('SELECT * FROM foo', { type: Sequelize.QueryTypes.SELECT });
			})
			.then((result: any[]) => {
				assert.equal(result.length, 0, 'should be an empty array');
			});
	});

	it('should initialise', (done) => {
		const associatedModel = (sequelize: any) => {
			const model = sequelize.define('foo');

			model.associate = () => done();

			return model;
		};
		instance.addModel(associatedModel);
		instance.initialise();
	});

	it('should access a transaction', () => {
		return instance.transaction((t: any) => {
			assert(t.commit, 'should be a transaction object');

			return Promise.resolve();
		});
	});
});

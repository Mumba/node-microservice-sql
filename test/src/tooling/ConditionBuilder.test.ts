/**
 * Remember to observe the 4 A's of testing and try to limit tests to just four calls:
 * - Arrange    - set up the system state
 * - Act        - do the thing we are testing
 * - Assert     - inspect the resulting state
 * - Annihilate - tear down
 *
 * @copyright 2017 Mumba Pty Ltd. All rights reserved.
 * @license   Apache-2.0
 */

import * as assert from 'assert';
import * as sequelize from 'sequelize';
import {ConditionBuilder} from '../../../src/index';
import {IncludeBuilder} from '../../../src/index';
const Op = sequelize.Op;

describe('ConditionBuilder', function () {
	let instance: ConditionBuilder;

	beforeEach(function () {
		instance = new ConditionBuilder();
	});

	it('should set some defaults', function () {
		const build = instance.build();

		assert.deepEqual(build, {});
	});

	describe('pagination', function () {
		it('should set the default limit if nothing passed', function () {
			assert.equal(instance.limit().build().limit, 10);
		});

		it('should set a limit', function () {
			assert.equal(instance.limit(12).build().limit, 12);
		});

		it('should set a zero offset', function () {
			assert.equal(instance.offset(0).build().offset, 0);
		});

		it('should set an offset', function () {
			assert.equal(instance.offset(2).build().offset, 2);
		});

		it('should set a page', function () {
			assert.equal(instance.limit(9).page(3).build().offset, 18);
		});
	});

	describe('where', function () {
		it('should build with 2 values', function () {
			assert.deepEqual(
				instance.where('foo', 'bar').build().where,
				{ foo: 'bar' }
			);
		});
	});

	describe('equals', function () {
		it('must be called after `where`', function () {
			assert.throws(function () {
				instance.equals('bar');
			}, /after where/);
		});

		it('should build', function () {
			assert.deepEqual(
				instance.where('foo').equals('bar').build().where,
				{ foo: 'bar' }
			);
		});
	});

	describe('attributes', function () {
		it('should set attributes', function () {
			assert.deepEqual(
				instance.attributes(['foo']).build().attributes,
				['foo']
			);
		});
	});

	describe('include', function () {
		it('should throw if object not an `IncludeBuilder`', function () {
			assert.throws(function () {
				instance.include(<IncludeBuilder>{});
			});
		});

		it('should include an association', function () {
			const Model = {};
			const include = new IncludeBuilder();

			include.model(Model).where('foo', 'bar');
			instance.include(include);

			assert.deepEqual(
				instance.build().include,
				[{ model: Model, where: { foo: 'bar' } }]
			);
		});

		it('should support nested includes', function () {
			const Model1 = { name: 'm1' };
			const Model2 = { name: 'm2' };
			const include1 = new IncludeBuilder();
			const include2 = new IncludeBuilder();

			include2.model(Model2).where('goo', 'car');

			include1.model(Model1)
				.where('foo', 'bar')
				.include(include2);

			instance.include(include1);

			assert.deepEqual(
				instance.build().include,
				[
					{
						model: Model1,
						where: { foo: 'bar' },
						include: [
							{
								model: Model2,
								where: { goo: 'car' }
							}
						]
					}
				]
			);

		});
	});

	describe('order', function () {
		it('should add ordering as a string', function () {
			assert.deepEqual(
				instance.order('title', 'DESC').build().order,
				[['title', 'DESC']]
			);
		});
	});

	describe('conditionals', function () {
		it('must be called after `where`', function () {
			assert.throws(function () {
				instance.conditional('foo', 12);
			}, /after where/);
		});

		it('gt', function () {
			assert.deepEqual(
				instance.where('foo').gt(12).build().where,
				{ foo: { [Op.gt]: 12 } }
			);
		});

		it('gte', function () {
			assert.deepEqual(
				instance.where('foo').gte(12).build().where,
				{ foo: { [Op.gte]: 12 } }
			);
		});

		it('lt', function () {
			assert.deepEqual(
				instance.where('foo').lt(12).build().where,
				{ foo: { [Op.lt]: 12 } }
			);
		});

		it('lte', function () {
			assert.deepEqual(
				instance.where('foo').lte(12).build().where,
				{ foo: { [Op.lte]: 12 } }
			);
		});

		it('ne', function () {
			assert.deepEqual(
				instance.where('foo').ne('bar').build().where,
				{ foo: { [Op.ne]: 'bar' } }
			);
		});

		it('in', function () {
			assert.deepEqual(
				instance.where('foo').in(['bar']).build().where,
				{ foo: { [Op.in]: ['bar'] } }
			);
		});

		it('notIn', function () {
			assert.deepEqual(
				instance.where('foo').notIn(['bar']).build().where,
				{ foo: { [Op.notIn]: ['bar'] } }
			);
		});

		it('compound', function () {
			assert.deepEqual(
				instance.where('foo').gt(6).lt(12).build().where,
				{ foo: { [Op.gt]: 6, [Op.lt]: 12 } }
			);
		});
	});

	describe('paranoid', function () {
		it('should build with paranoid', function () {
			assert.strictEqual(
				instance.paranoid(false).build().paranoid,
				false
			);
		});
	});
});

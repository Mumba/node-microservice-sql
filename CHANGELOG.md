# Change Log
All notable changes to this project will be documented in this file.
This project adheres to [Semantic Versioning](http://semver.org/).

## [Unreleased]

## [0.5.11] 10 Apr 2018
- Added `set` to `ConditionBuilder` to allow for adhoc properties.

## [0.5.10] 25 Oct 2017
- Updated deps; adjusted `ConditionBuilder.where` to support Symbol keys.

## [0.5.9] 21 Oct 2017
- Added `options` to `SqlDataservice.add` and `SqlDataservice.set`.

## [0.5.8] 11 Oct 2017
- Fixed SqlDataservice.getFindAllConditions to work with custom primary key.

## [0.5.7] 11 Oct 2017
- Allowed SqlDataservice to support a custom primary key name.

## [0.5.6] 11 Oct 2017
- Updated deps; change to Sequelize operators.

## [0.5.5] 1 Sep 2017
- Updated deps; added `paranoid` option to `ConditionBuilder`.

## [0.5.4] 9 Aug 2017
- Added `count` method to `SqlDataService`.

## [0.5.3] 8 Aug 2017
- Added db initialisation to associate models.

## [0.5.2] 26 Jul 2017
- Updated deps.
- Added `delete` method to `SqlDataService`.

## [0.5.1] 30 Jun 2017
- Updated `dbmigrate` to `mysql2`.

## [0.5.0] 29 Jun 2017
- Updated to Sequelize 4; added linting.

## [0.4.1] 20 Feb 2017
- Split data service validation into `validateAdd` and `validateSet`.

## [0.4.0] 20 Feb 2017
- Changed `SqlDataService.validate` to take a model Instance when setting. 

## [0.3.1] 10 Feb 2017
- Added method to `SqlDataService.validate`.

## [0.3.0] 9 Feb 2017
- Updated deps; added `SqlDataService`.

## [0.2.0] 15 Aug 2016
- Updated Vault configuration. Added command-line args to `dbmigrate`.

## [0.1.6] 11 Aug 2016
- Added option to allow Vault host to be passed into `dbmigrate`.

## [0.1.5] 3 Aug 2016
- Allowed `offset` in `ConditionBuilder` to be explicitly set to zero.

## [0.1.4] 3 Aug 2016
- Added `createStream` function to allow a Sequelize query to be streamed in batches.

## [0.1.3] 1 Aug 2016
- Added `dbmigrate` tooling for Knex migrations.

## [0.1.2] 1 Aug 2016
- Fix typedefs signatures in ConditionBuilder for greater than and less than methods. Added `Date` this time.

## [0.1.1] 29 Jul 2016
- Fix typedefs signatures in ConditionBuilder for greater than and less than methods. Added `string`.

## [0.1.0] 28 Jul 2016
- Initial release
